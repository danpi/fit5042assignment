package assignment.controllers;

import java.io.Serializable;


import javax.el.ELContext;


import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import assignment.repository.entities.Customer;
import assignment.controllers.AUSApplication;
import assignment.mbeans.CustomerManagedBean;


@RequestScoped
@Named("removeCustomer")
public class RemoveCustomer {

    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    private boolean showForm = true;

    
    private Customer customer;

    AUSApplication app;

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public RemoveCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (AUSApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "ausApplication");

        app.updateCustomerList();

        //instantiate propertyManagedBean
        //ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(context, null, "customerManagedBean");
    }

    /**
     * @param 
     */
    public void removeCustomer(int customerID) {
        try {
            //remove this property from db via EJB
           customerManagedBean.removeCustomer(customerID);

            //refresh the list in PropertyApplication bean
             app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;

    }

}