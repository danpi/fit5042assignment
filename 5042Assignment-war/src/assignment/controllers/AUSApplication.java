package assignment.controllers;

import javax.inject.Named;


import java.util.ArrayList;
import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import assignment.mbeans.ContactManagedBean;
import assignment.mbeans.CustomerManagedBean;
import assignment.repository.entities.ContactPerson;
import assignment.repository.entities.Customer;

@Named(value = "ausApplication")
@ApplicationScoped
public class AUSApplication {
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;
    ContactManagedBean contactManagedBean;
    
    private ArrayList<Customer> customers;
     
    private boolean showForm = true;

    public boolean isShowForm() {
        return showForm;
    }
    
    public AUSApplication() throws Exception {
    	customers = new ArrayList<>();
    	
      
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");

       
        updateCustomerList();
        
    }
    
    public Customer getCustomerByID(int customerID) {
    	Customer customer = new Customer();
    	for(Customer cus:getCustomers()) {
    		if (cus.getCustomerID() == customerID) {
    			return cus;
    		}
    	}
		return customer;
    }
    
    public ContactPerson getContactByID(int contactIndex, Customer customer) {
    	ContactPerson contactPerson = new ContactPerson();
    	for(ContactPerson con:customer.getContacts()) {
    		if (con.getConactPersonId() == contactIndex) {
    			return con;
    		}
    	}
		return contactPerson;
    }

	/**
	 * @return the customers
	 */
	public ArrayList<Customer> getCustomers() {
		//updateCustomerList();
		return customers;
	}

	
	
	
	/**
	 * @param customers the customers to set
	 */
	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}


	
	public void updateCustomerList() {
      if (customers != null && customers.size() > 0)
      {
          
      }
      else
      {
      	customers.clear();

          for (Customer customer : customerManagedBean.getAllCustomers())
          {
          	customers.add(customer);
          }

          setCustomers(customers);
      }
}
	
	
	
	public void searchCustomerById(int CustomerID) {
		customers.clear();
		
        customers.add(customerManagedBean.searchPropertyById(CustomerID));
    }

    public void searchAll()
    {
    	customers.clear();
        
        for (Customer customer : customerManagedBean.getAllCustomers())
        {
        	customers.add(customer);
        }
        
        setCustomers(customers);
    }
	

	
	
}
