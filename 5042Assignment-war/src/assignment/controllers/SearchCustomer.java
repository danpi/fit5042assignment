package assignment.controllers;

import javax.el.ELContext;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import assignment.repository.entities.Customer;

@RequestScoped
@Named("searchCustomer")
public class SearchCustomer {
	private boolean showForm = true;
	
	private Customer customer;
	AUSApplication app;
	private int searchByInt;
    //private double searchByDouble;
	
	public boolean isShowForm() {
        return showForm;
    }
	
	/**
	 * @return the app
	 */
	public AUSApplication getApp() {
		return app;
	}
	
	/**
	 * @param app the app to set
	 */
	public void setApp(AUSApplication app) {
		this.app = app;
	}

	public int getSearchByInt() {
        return searchByInt;
    }

    public void setSearchByInt(int searchByInt) {
        this.searchByInt = searchByInt;
    }
	
	
	
	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	public SearchCustomer() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (AUSApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "ausApplication");

		app.updateCustomerList();
	}

	public void searchCustomerById(int customerID) {
        try {
          
            app.searchCustomerById(customerID);
        } catch (Exception ex) {

        }
        showForm = true;

    }
	
	
    public void searchAll() {
        try {
            
            app.searchAll();
        } catch (Exception ex) {

        }
        showForm = true;
    }

}
