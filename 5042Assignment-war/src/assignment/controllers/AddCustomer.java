package assignment.controllers;

import javax.el.ELContext;


import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import assignment.repository.entities.Address;
import assignment.repository.entities.Customer;
import assignment.controllers.AUSApplication;
import assignment.mbeans.CustomerManagedBean;


@RequestScoped
@Named("addCustomer")
public class AddCustomer {

    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;

    private boolean showForm = true;

    private Customer customer;

    AUSApplication app;

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public boolean isShowForm() {
        return showForm;
    }

    public AddCustomer() {
        ELContext context
                = FacesContext.getCurrentInstance().getELContext();

        app = (AUSApplication) FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "AUSApplication");
        customer = new Customer();
        customer.setAddress(new Address());
        //instantiate propertyManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
    }

    public void addCustomer() {
        //this is the local property, not the entity
        try {
            //add this property to db via EJB
        	customerManagedBean.addCustomer(customer);

            //refresh the list in PropertyApplication bean
             app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been added succesfully"));
        } catch (Exception ex) {

        }
        showForm = true;
    }

}
