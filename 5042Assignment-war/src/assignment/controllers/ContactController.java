package assignment.controllers;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Level;
import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import assignment.mbeans.ContactManagedBean;
import assignment.mbeans.CustomerManagedBean;
import assignment.repository.entities.ContactPerson;
import assignment.repository.entities.Customer;

@SessionScoped
@Named(value = "addContactPerson")

public class ContactController implements Serializable{

	@ManagedProperty(value = "#{contactManagedBean}")
	ContactManagedBean contactManagedBean;
	

	private AUSApplication app;
	private int customerid;
	private Customer customer;
	private ContactPerson contactPerson;
	private int contactIndex;
	
	
	public ContactController() {
		// instantiate customerManagedBean
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		contactManagedBean = (ContactManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "contactManagedBean");
		app = (AUSApplication) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "ausApplication");

        
		contactPerson = new ContactPerson();
		
		customerid = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerID"));
		setCustomer(app.getCustomerByID(customerid));
		
		contactIndex = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("contactIndex"));
		setContactPerson(app.getContactByID(contactIndex,customer));
	}
	
	
	
	public void addContactPerson() {
		try {
			contactPerson.setCustomer(getCustomer());
			contactManagedBean.addContactPerson(contactPerson);

			app.searchAll();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact Person has been added succesfully"));
		} catch (Exception ex) {
			Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}


    public void removeContactPerson(int customerID) {
        
    	try {
            
           contactManagedBean.removeContact(customerID);

           
             app.searchAll();

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully"));
        } catch (Exception ex) {

        }
        

        app.updateCustomerList();

        
        
    }

   
	/**
	 * @return the customerid
	 */
	public int getCustomerid() {
		return customerid;
	}

	
	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	/**
	 * @return the contactPerson
	 */
	public ContactPerson getContactPerson() {
		return contactPerson;
	}

	/**
	 * @param contactPerson the contactPerson to set
	 */
	public void setContactPerson(ContactPerson contactPerson) {
		this.contactPerson = contactPerson;
	}

//	public void searchAll()
//    {
//    	//customers.clear();
//        
//        for (ContactPerson contactPerson : contactManagedBean.getAllContacts())
//        {
//        	contactPerson.add(contactPerson);
//        }
//        
//        setCustomers(customers);
//    }
//	
//	


}
