package assignment.controllers;

import java.io.Serializable;
import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import assignment.repository.entities.Customer;

@Named(value = "customerController")
@RequestScoped
public class CustomerController {

	private int customerIndex;
	private Customer customer;
	

//	private ArrayList<String> industries;
	// private String selectedIndestry;

	public CustomerController() {

		customerIndex = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerIndex"));
		customer = initCustomer();
		

	}

	public int getCustomerIndex() {
		return customerIndex;
	}

	public void setCustomerIndex(int customerIndex) {
		this.customerIndex = customerIndex;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {

        return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Customer initCustomer() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();
		AUSApplication app = (AUSApplication) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(context, null, "ausApplication");
		return app.getCustomerByID(customerIndex);
	}
}
