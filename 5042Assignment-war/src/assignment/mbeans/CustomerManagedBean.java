package assignment.mbeans;

import java.io.Serializable;


import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import assignment.repository.CustomerRepository;
import assignment.repository.entities.Customer;
import assignment.mbeans.CustomerManagedBean;
import assignment.repository.entities.Address;

import assignment.repository.entities.ContactPerson;

@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable{
	
    @EJB
    CustomerRepository customerRepository;
    
    public CustomerManagedBean() {
    }
	
    public List<Customer> getAllCustomers() {
        try {
            List<Customer> customers = customerRepository.getAllCustomers();
            return customers;
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void addCustomer(Customer customer) {
        try {
        	customerRepository.addCustomer(customer);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
  
//  //
//  public void addCustomer(Customer localCustomer) {
//      //convert this newProperty which is the local property to entity property
//  	Customer customer = convertCustomerToEntity(localCustomer);
//
//      try {
//      	customerRepository.addCustomer(customer);
//      } catch (Exception ex) {
//          Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
//      }
//  }
  

    public void removeCustomer(int customerID) {
        try {
            customerRepository.removeCustomer(customerID);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editCustomer(Customer customer) {
        try {
            String s = customer.getAddress().getStreetNumber();
            Address address = customer.getAddress();
            address.setStreetNumber(s);
            customer.setAddress(address);

            customerRepository.editCustomer(customer);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Property has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Search a property by Id
     */
    public Customer searchPropertyById(int id) {
        try {
            return customerRepository.searchCustomerById(id);
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
//
//    public Set<Customer> searchCustomerByContactPerson(ContactPerson contactPerson) {
//        try {
//            return customerRepository.searchCustomerByContactPerson(contactPerson);
//        } catch (Exception ex) {
//            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return null;
//    }

    public List<ContactPerson> getAllCustomerContact() throws Exception {
        try {
            return customerRepository.getAllContactPeople();
        } catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
    
    

    
/**
    public List<Customer> searchPropertyByBudget(double budget) {
        try {
            return customerRepository.searchCustomerByBudget(budget);
        } catch (Exception ex) {
            Logger.getLogger(PropertyManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
*/

    
/**
    private Property convertPropertyToEntity(fit5042.tutex.controllers.Property localProperty) {
        Property property = new Property(); //entity
        String streetNumber = localProperty.getStreetNumber();
        String streetAddress = localProperty.getStreetAddress();
        String suburb = localProperty.getSuburb();
        String postcode = localProperty.getPostcode();
        String state = localProperty.getState();
        Address address = new Address(streetNumber, streetAddress, suburb, postcode, state);
        property.setAddress(address);
        property.setNumberOfBedrooms(localProperty.getNumberOfBedrooms());
        property.setPrice(localProperty.getPrice());
        property.setSize(localProperty.getSize());
        property.setPropertyId(localProperty.getPropertyId());
        property.setTags(localProperty.getTags());
        int conactPersonId = localProperty.getConactPersonId();
        String name = localProperty.getName();
        String phoneNumber = localProperty.getPhoneNumber();
        ContactPerson contactPerson = new fit5042.tutex.repository.entities.ContactPerson(conactPersonId, name, phoneNumber);
        if (contactPerson.getConactPersonId() == 0) {
            contactPerson = null;
        }
        property.setContactPerson(contactPerson);
        return property;
    }
*/
}