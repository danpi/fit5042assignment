
package assignment.mbeans;

import java.io.Serializable;


import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import assignment.repository.ContactRepository;
import assignment.repository.CustomerRepository;
import assignment.repository.entities.Customer;
import assignment.mbeans.CustomerManagedBean;
import assignment.repository.entities.Address;

import assignment.repository.entities.ContactPerson;

@ManagedBean(name = "contactManagedBean")
@SessionScoped
public class ContactManagedBean implements Serializable{
	
    @EJB
    ContactRepository contactRepository;
    
    public ContactManagedBean() {
    }
    
  
    public List<ContactPerson> getAllContacts() {
        try {
            List<ContactPerson> contact = contactRepository.getAllContacts();
            return contact;
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void addContactPerson(ContactPerson contactPerson) {
        try {
        	
        	contactRepository.addContactPerson(contactPerson);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   

    public void removeContact(int conactPersonId) {
        try {
        	contactRepository.removeContactPerson(conactPersonId);
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//
    public void editContactPerson(ContactPerson contact) {
        try {
        
            String s = contact.getPhoneNumber();
            contact.setPhoneNumber(s);
            
            contactRepository.editContactPerson(contact);

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact Phone Number has been updated succesfully"));
        } catch (Exception ex) {
            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//  
//   
//    public Customer searchContactById(int id) {
//        try {
//            return contactRepository.searchContactById(id);
//        } catch (Exception ex) {
//            Logger.getLogger(ContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return null;
//    }
    


}