package assignment.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import assignment.repository.entities.ContactPerson;
import assignment.repository.entities.Customer;

@Stateless
public class ContactPersonRepositoryImpl implements ContactRepository {

	@PersistenceContext(unitName = "5042Assignment-ejb") // unitName = persistence-unit in
															// jebModule/META-INF/persistence.xml
	private EntityManager entityManager;

	@Override
	public List<ContactPerson> getAllContacts() throws Exception {
		return entityManager.createNamedQuery(ContactPerson.GET_ALL_CONTACT_PERSON).getResultList();	
	}
	
	
	@Override
	public void addContactPerson(ContactPerson contactPerson) throws Exception {
		List<ContactPerson> contactPersons = entityManager.createNamedQuery(ContactPerson.GET_ALL_CONTACT_PERSON)
				.getResultList();
		if (contactPersons.size()==0) {
			contactPerson.setConactPersonId(1);
		}else {
			contactPerson.setConactPersonId(contactPersons.get(0).getConactPersonId() + 1);
		}
		
		Customer customer = contactPerson.getCustomer();
		customer.getContacts().add(contactPerson);
		entityManager.merge(customer);
	}
	
	@Override
    public void removeContactPerson(int conactPersonId) throws Exception {
        //complete this method
		ContactPerson contactPerson = this.searchContactById(conactPersonId);

        if (contactPerson != null) {
            entityManager.remove(contactPerson);
        }
    }

    @Override
    public void editContactPerson(ContactPerson contactPerson) throws Exception {
        try {
            entityManager.merge(contactPerson);
        } catch (Exception ex) {

        }
    }
    
    @Override
    public ContactPerson searchContactById(int conactPersonId) throws Exception {
    	ContactPerson contactPerson = entityManager.find(ContactPerson.class, conactPersonId);
    	//customer.getTags();
        return contactPerson;
    }
}
