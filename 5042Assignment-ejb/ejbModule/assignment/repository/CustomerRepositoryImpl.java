package assignment.repository;

import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import assignment.repository.CustomerRepository;
import assignment.repository.entities.Customer;
import assignment.repository.entities.ContactPerson;


@Stateless
public class CustomerRepositoryImpl implements CustomerRepository {

	@PersistenceContext (unitName = "5042Assignment-ejb")
	private EntityManager entityManager;
	
	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();	
	}
	
	@Override
    public void addCustomer(Customer customer) throws Exception {
        List<Customer> customers = entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();
        customer.setCustomerID(customers.get(0).getCustomerID() + 1);
        entityManager.persist(customer);
    }
	
	@Override
    public void removeCustomer(int customerID) throws Exception {
        //complete this method
		Customer customer = this.searchCustomerById(customerID);

        if (customer != null) {
            entityManager.remove(customer);
        }
    }

    @Override
    public void editCustomer(Customer customer) throws Exception {
        try {
            entityManager.merge(customer);
        } catch (Exception ex) {

        }
    }
    
    @Override
    public Customer searchCustomerById(int id) throws Exception {
    	Customer customer = entityManager.find(Customer.class, id);
    	//customer.getTags();
        return customer;
    }
    
   
	
//    @Override
//    public Set<Customer> searchCustomerByContactPerson(ContactPerson contactPerson) throws Exception {
//        contactPerson = entityManager.find(ContactPerson.class, contactPerson.getConactPersonId());
//        contactPerson.getCustomer().size();
//        entityManager.refresh(contactPerson);
//
//        return contactPerson.getCustomer();
//    }
//    
    @Override
    public List<ContactPerson> getAllContactPeople() throws Exception {
        return entityManager.createNamedQuery(ContactPerson.GET_ALL_CONTACT_PERSON).getResultList();
    }

}
