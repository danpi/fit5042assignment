package assignment.repository;

import javax.ejb.Remote;

import java.util.List;
import java.util.Set;

import assignment.repository.entities.Customer;
import assignment.repository.entities.ContactPerson;

@Remote
public interface ContactRepository {
	
    public List<ContactPerson> getAllContacts() throws Exception;
    
    public void addContactPerson(ContactPerson contactPerson) throws Exception;
    
    public void removeContactPerson(int conactPersonId) throws Exception;
//    
    public void editContactPerson(ContactPerson contactPerson) throws Exception;
//    
    public ContactPerson searchContactById(int conactPersonId) throws Exception;
    
 //   public List<ContactPerson> getAllContactPeople() throws Exception;

    
   // public List<Customer> searchCustomerByBudget(double budget) throws Exception;
}