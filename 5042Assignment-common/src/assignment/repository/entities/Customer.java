package assignment.repository.entities;

import java.io.Serializable;

import java.util.Set;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import assignment.repository.entities.Customer;
import assignment.repository.entities.Address;
import assignment.repository.entities.ContactPerson;


@Entity
@NamedQueries({
	@NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c order by c.customerID") })

public class Customer implements Serializable {
	public static final String GET_ALL_QUERY_NAME =  "Customer.getAll";
	
	private int customerID;
	private double size;
	private String firstName;
	private String lastName;
	private Set<ContactPerson> contacts;
	private Address address;
   
	
	public Customer() {
		
	}
	
	/**
	 * @param customerID
	 * @param firstName
	 * @param lastName
	 */
	public Customer(int customerID, String firstName,Double size, String lastName, Address address) {
		this.customerID = customerID;
		
		this.firstName = firstName;
		this.lastName = lastName;
		contacts = new HashSet<>();
		this.address = address;
	}

	/**
	 * @return the customerID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "customer_id")
	public int getCustomerID() {
		return customerID;
	}

	/**
	 * @param customerID the customerID to set
	 */
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Embedded
    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
	
	
  
	/**
	 * @return the contacts
	 */
	@OneToMany(mappedBy = "customer",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public Set<ContactPerson> getContacts() {
		return contacts;
	}

	
	public void setContacts(Set<ContactPerson> contacts) {
		this.contacts = contacts;
	}

	@Override
	public String toString() {
		return "Customer [customerID=" + customerID + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
	
}

