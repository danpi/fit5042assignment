package assignment.repository;

import javax.ejb.Remote;

import java.util.List;
import java.util.Set;

import assignment.repository.entities.Customer;
import assignment.repository.entities.ContactPerson;
import assignment.repository.entities.Customer;

@Remote
public interface CustomerRepository {
	
    public List<Customer> getAllCustomers() throws Exception;

    
    public void addCustomer(Customer customer) throws Exception;
    
    public void removeCustomer(int customerID) throws Exception;
    
    public void editCustomer(Customer customer) throws Exception;
    
    
    public Customer searchCustomerById(int id) throws Exception;
    
    
 //   Set<Customer> searchCustomerByContactPerson(ContactPerson contactPerson) throws Exception;

  
    public List<ContactPerson> getAllContactPeople() throws Exception;

   
    
   // public List<Customer> searchCustomerByBudget(double budget) throws Exception;



}